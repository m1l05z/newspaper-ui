angular
  .module('app')
  .controller('UserSignUpController', UserSignUpController);

UserSignUpController.$inject = ['$scope', '$location','UserService', 'MessageService'];

function UserSignUpController($scope, $location, UserService, MessageService) {
  var vm = this;
  $scope.message = MessageService;

  vm.signUp = signUp;

  function signUp(user) {
    console.log(user);

    var l = Ladda.create($('#register-submit')[0]);
    l.start();

    var request = UserService.signUp(user)

    request.done(function(data) {
      console.log(data);
      console.log(data.user.authentication_token);
      UserService.setUser(data.user);
      $location.path('/settings');
      $scope.$apply();
      l.stop();
    });

    request.fail(function($xhr, textStatus) {
      var data = $xhr.responseJSON;
      MessageService.errors = data.errors;
      $scope.$apply();
      MessageService.errors = null;
      console.log(data);
      l.stop();
    });
  }
}
