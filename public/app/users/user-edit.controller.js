angular
  .module('app')
  .controller('UserEditController', UserEditController);

UserEditController.$inject = ['$scope', '$location', '$routeParams', 'UserService', 'MessageService', 'CityService', 'store'];

function UserEditController($scope, $location, $routeParams, UserService, MessageService, CityService, store) {
  var vm = this;
  $scope.message = MessageService;
  $scope.oneAtATime = true;

  vm.update = update;
  vm.initLocation = initLocation;
  vm.user = UserService.getUser();
  vm.cities = null;

  function init() {
    var user = UserService.getUser();
    var request = CityService.cities(user.authentication_token);

    request.then(function(data) {
      console.log(data);
      vm.cities = data.cities;
    }, function($xhr, textStatus) {
      var data = $xhr.responseJSON;
      console.log(data);
    });
  }

  init();

  function update(user) {
    console.log(user);

    var l = Ladda.create($('#user-edit-submit')[0]);
    l.start();

    var request = UserService.update(user)

    request.done(function(data) {
      console.log(data);
      UserService.setUser(data.user);
      $scope.$apply();
      l.stop();
    });

    request.fail(function($xhr, textStatus) {
      var data = $xhr.responseJSON;
      MessageService.errors = data.errors;
      $scope.$apply();
      console.log(data);
      l.stop();
    });
  }

  function initLocation() {
    $.geolocation.get({win: setLocation, fail: console.log("Fail Location")});
  }

  function setLocation(position) {
    var l = Ladda.create($('#getPosition')[0]);
    l.start();
    
    var request = UserService.sendLocation(
      UserService.getUser().authentication_token,
      position.coords.latitude,
      position.coords.longitude)

    request.done(function(data) {
      console.log(data);
      UserService.setUser(data.user);
      vm.user.city = data.user.city;
      $scope.$apply();
      l.stop();
    });

    request.fail(function($xhr, textStatus) {
      var data = $xhr.responseJSON;
      MessageService.errors = data.errors;
      $scope.$apply();
      console.log(data);
      l.stop();
    });
  }
}
