angular
  .module('app')
  .controller('UserSignInController', UserSignInController);

UserSignInController.$inject = ['$scope', '$location', '$routeParams', 'auth', 'store','UserService', 'MessageService'];

function UserSignInController($scope, $location, $routeParams, auth, store, UserService, MessageService) {
  var vm = this;

  vm.signIn = signIn;
  vm.facebookLogin = facebookLogin;
  vm.twitterLogin = twitterLogin;
  vm.googleLogin = googleLogin;
  $scope.message = MessageService;

  function signIn(user) {
    console.log(user);

    var l = Ladda.create($('#login-submit')[0]);
    l.start();

    var request = UserService.signIn(user)

    request.done(function(data) {
      console.log(data);
      $location.path('/user/edit');
      $scope.$apply();
      l.stop();
    });

    request.fail(function($xhr, textStatus) {
      var data = $xhr.responseJSON;
      MessageService.errors = data.errors;
      $scope.$apply();
      MessageService.errors = null;
      console.log(data);
      l.stop();
    });
  }

  function onLoginSuccess(profile, token) {
    console.log(profile);
    var request = UserService.signInSocial(auth.profile.identities[0].user_id, auth.profile.identities[0].access_token, auth.profile.identities[0].provider)

    request.done(function(data) {
      console.log(data);
      $location.path('/settings');
      $scope.$apply();
    });

    request.fail(function($xhr, textStatus) {
      var data = $xhr.responseJSON;
      MessageService.errors = data.errors;
      $scope.$apply();
      console.log(data);
    });
  }

  function onLoginFailed(message) {
    console.log('failed');
    console.log(message);
  }

  function facebookLogin() {
    auth.signin({
      popup: true,
      connection: 'facebook',
      scope: 'openid name email'
    }, onLoginSuccess, onLoginFailed);
  }

  function twitterLogin() {
    auth.signin({
      popup: true,
      connection: 'twitter',
      scope: 'openid name email'
    }, onLoginSuccess, onLoginFailed);
  }

  function googleLogin() {
    auth.signin({
      popup: true,
      connection: 'google-oauth2',
      scope: 'openid name email'
    }, onLoginSuccess, onLoginFailed);
  }
}
