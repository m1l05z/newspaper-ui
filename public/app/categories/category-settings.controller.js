angular
  .module('app')
  .controller('CategorySettingsController', CategorySettingsController);

CategorySettingsController.$inject = ['$scope', '$location', '$routeParams', 'UserService', 'CategoryService'];

function CategorySettingsController($scope, $location, $routeParams, UserService, CategoryService) {
  var vm = this;

  vm.categories = [];
  vm.selectedCategoryIds = [];
  vm.update = update;

  function init() {
    var user = UserService.getUser();
    var request = CategoryService.categories(user.authentication_token);

    request.then(function(data) {
      console.log(data);
      vm.categories = data.categories;

      var request2 = CategoryService.selectedCategories(user.authentication_token);

      request2.then(function(data) {
        console.log(data);

        vm.selectedCategoryIds = data.user.selected_category_ids;

        vm.categories = $.map(vm.categories, function(category) {
          var selected = false;

          $.each(vm.selectedCategoryIds, function(index, c) {
            if(category.id == c) {
              selected = true;
              return false
            }
          });

          return {
            id: category.id,
            name: category.name,
            selected: selected
          }
        });
      }, function($xhr, textStatus) {
        var data = $xhr.responseJSON;
        console.log(data);
        $location.path('/sign_out');
      });
    }, function($xhr, textStatus) {
      var data = $xhr.responseJSON;
      console.log(data);
      $location.path('/sign_out');
    });
  }

  init();

  function update() {
    var user = UserService.getUser();
    var categories = $.grep(vm.categories, function(e) { return e.selected; });

    categories = $.map(categories, function(category) {
      return category.id
    });

    var l = Ladda.create($('#user-selected-category-submit')[0]);
    l.start();

    var request = CategoryService.update(user.authentication_token, categories);

    request.done(function(data) {
      console.log(data);
      $location.path('/');
      $scope.$apply();
      l.stop();
    });

    request.fail(function($xhr, textStatus) {
      var data = $xhr.responseJSON;
      console.log(data);
      l.stop();
    });
  }
}
