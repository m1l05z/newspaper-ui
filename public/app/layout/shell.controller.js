angular
  .module('app')
  .controller('ShellController', ShellController);

ShellController.$inject = ['$q', '$scope', '$location', '$routeParams', 'UserService', 'ArticleService', 'WeatherService'];

function ShellController($q, $scope, $location, $routeParams, UserService, ArticleService, WeatherService) {
  var vm = this;

  vm.actualTime = startTime();
  vm.date = null;
  vm.year = null;
  vm.month = null;
  vm.week = null;
  vm.day = null;
  vm.dayWeek = null;
  vm.dayWeekWord = null;
  vm.news = null;
  vm.weather = null;
  vm.articlesLoading = false;
  vm.weatherLoading = false;

  function init() {
    var now = new Date($.now());
    vm.year = now.getFullYear();
    vm.month = now.getMonth() + 1;
    var monthWords = ['Styczeć', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'];
    vm.monthWord = monthWords[vm.month-1];
    vm.week = '????';
    vm.day = now.getUTCDate();
    vm.dayWeek = now.getDay();
    var dayWords = ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'];
    vm.dayWeekWord = dayWords[vm.dayWeek];

    var user = UserService.getUser();
    var request = ArticleService.articles(user.authentication_token);

    vm.articlesLoading = true;

    request.then(function(data) {
      vm.articlesLoading = false;
      vm.news = data.news;
    }, function(data) {
      vm.articlesLoading = false;
      $location.path('/sign_out');
    });

    vm.weatherLoading = true;

    var request2 = WeatherService.getWeather('Rumia');
    request2.then(function(data) {
      vm.weatherLoading = false;
      vm.weather  = data;
    }, function() {
      vm.weatherLoading = false;
      alert('Nie pobrano danych pogodowych.');
    });
  }

  init();
}
