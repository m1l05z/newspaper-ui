angular.module('app', [  
  'ngAnimate',
  'ngRoute',
  'ngSanitize',
  'auth0',
  'angular-storage',
  'angular-jwt',
  'ui.bootstrap'
]);
