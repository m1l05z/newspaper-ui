angular
  .module('app')
  .service('ArticleService', ArticleService);

function ArticleService($q) {
  this.articles = function(token) {
    var deferred = $q.defer();

    $.ajax({
      method: "GET",
      url: 'https://newspaper-api.herokuapp.com/v1/articles',
      data: {token: token}
    }).done(function(data) {
      deferred.resolve(data);
    }).fail(function(data) {
      deferred.reject(data);
    });

    return deferred.promise;
  };
}
