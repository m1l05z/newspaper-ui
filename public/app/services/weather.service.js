angular
  .module('app')
  .service('WeatherService', WeatherService);

WeatherService.$inject = ['$q', 'store'];

function WeatherService($q, store) {
  this.getWeather = function(city) {
    var deferred = $q.defer();
    var user = store.get('user');

    $.ajax({
      method: "GET",
      url: 'https://newspaper-api.herokuapp.com/v1/users/weather',
      data: {token: user.authentication_token}
    }).then(function(data) {
      deferred.resolve(data);
      console.log(data);
    }).fail(function(data) {
      deferred.reject(data);
    });
    
    return deferred.promise;
  };
}
