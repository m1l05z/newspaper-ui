angular
  .module('app')
  .service('CityService', CityService);

function CityService($q) {
  this.cities = function(token) {
    var deferred = $q.defer();

    $.ajax({
      method: "GET",
      url: 'https://newspaper-api.herokuapp.com/v1/cities',
      data: {token: token}
    }).done(function(data) {
      deferred.resolve(data);
    }).fail(function(data) {
      deferred.reject(data);
    });
    
    return deferred.promise;
  }
}
