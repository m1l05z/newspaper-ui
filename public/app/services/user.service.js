angular
  .module('app')
  .service('UserService', UserService);

UserService.$inject = ['store'];

function UserService(store) {
  var us = this;
  us._user = null;

  us.setUser = function(user) {
    us._user = user;
    store.set('user', us._user);
  }

  us.isUser = function() {
    if(us._user === null) return false;
    return true;
  }

  us.getUser = function() {
    if(!us.isUser()) {
      var user = store.get('user');
      if(user === null) return null;

      us._user = user;
      return user;
    }

    return us._user;
  }

  us.signIn = function(user) {
    return $.ajax({
      method: "POST",
      url: 'https://newspaper-api.herokuapp.com/v1/users/sign_in',
      data: {user: user}
    }).done(function(data) {
      us.setUser(data.user);
    });
  };

  us.signInSocial = function(id, token, provider) {
    return $.ajax({
      method: "POST",
      url: 'https://newspaper-api.herokuapp.com/v1/users/sign_in/'+provider,
      data: {user_id: id, token: token}
    }).done(function(data) {
      us.setUser(data.user);
    });
  };

  this.signUp = function(user) {
    return $.ajax({
      method: "POST",
      url: 'https://newspaper-api.herokuapp.com/v1/users',
      data: {user: user}
    });
  };

  this.signOut = function(user) {
    us.setUser(null);
  };

  this.update = function(user) {
    return $.ajax({
      type: "POST",
      url: 'https://newspaper-api.herokuapp.com/v1/users/' + us.getUser().authentication_token,
      data: {_method:'PUT', user: user}
    }).done(function(data) {
      us.setUser(data.user);
    });
  };

  this.sendLocation = function(token, latitude, longitude) {
    return $.ajax({
      type: "POST",
      url: 'https://newspaper-api.herokuapp.com/v1/users/location/' + token,
      data: {latitude: latitude, longitude: longitude}
    });
  };
}
