angular
  .module('app')
  .service('CategoryService', CategoryService);

function CategoryService($q) {
  this.categories = function(token) {
    var deferred = $q.defer();

    $.ajax({
      method: "GET",
      url: 'https://newspaper-api.herokuapp.com/v1/categories',
      data: {token: token}
    }).done(function(data) {
      deferred.resolve(data);
    }).fail(function(data) {
      deferred.reject(data);
    });
    
    return deferred.promise;
  }

  this.selectedCategories = function(token) {
    var deferred = $q.defer();

    $.ajax({
      method: "GET",
      url: 'https://newspaper-api.herokuapp.com/v1/users/selected_categories',
      data: {token: token}
    }).done(function(data) {
      deferred.resolve(data);
    }).fail(function(data) {
      deferred.reject(data);
    });
    
    return deferred.promise;
  }

  this.update = function(token, categories) {
    return $.ajax({
      type: "POST",
      url: 'https://newspaper-api.herokuapp.com/v1/users/selected_categories',
      data: {_method:'PUT', token: token, selected_category_ids: categories}
    });
  }
}
