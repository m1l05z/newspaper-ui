angular
  .module('app')
  .config(config)
  .run(run);

function config($routeProvider, $locationProvider, authProvider) {
  authProvider.init({
    domain: 'newspaper.eu.auth0.com',
    clientID: 'jFDTwha1bUvQa7y7Qv335SWuasmxNCgC',
    secret: 'ki2W1vIDECIJjwm3OVMYrXktyXq44GQsO8NiwmoDCJLSwA77ZnYCfcJV_ehTVX2B',
    callbackURL: 'http://ruby-box-151724.euw1.nitrousbox.com/#/sign_in'
  });

  var requireAuthentication = function () {
    return {
      load: function ($q, $location, UserService) {
        console.log('Can user access route?');
        var deferred = $q.defer();
        deferred.resolve();

        var user = UserService.getUser();

        if (user !== null && user.authentication_token !== null) {
          console.log('Yes they can!');
          return deferred.promise;
        }
        else {
          console.log('No they cant!');
          $location.path('/sign_in');
        }
      }
    };
  };
  
  var removeToken = function () {
    return {
      load: function ($q, $location, UserService) {
        console.log('test sign out');
        var deferred = $q.defer();
        UserService.signOut();
        $location.path('/sign_in');
      }
    };
  };

  $routeProvider

  .when('/', {
    templateUrl: 'app/layout/shell.html',
    controller: 'ShellController',
    controllerAs: 'vm',
    resolve: requireAuthentication()
  })

  .when('/sign_in', {
    templateUrl: 'app/users/user-sign-in.html',
    controller: 'UserSignInController',
    controllerAs: 'vm'
  })

  .when('/sign_up', {
    templateUrl: 'app/users/user-sign-up.html',
    controller: 'UserSignUpController',
    controllerAs: 'vm'
  })

  .when('/sign_out', {
    resolve: removeToken()
  })

  .when('/user/edit', {
    templateUrl: 'app/users/user-edit.html',
    controller: 'UserEditController',
    controllerAs: 'vm',
    resolve: requireAuthentication()
  })

  $locationProvider.html5Mode(false);
}

function run(auth) {
  auth.hookEvents();
}
